
# vresod.github.io

My personal website!

## Site Facts

### Home Page

This part of the site is where you land when you go to [the site](https://vresod.github.io). Not  much can be said except for the inclusion of a small Q&A.

### Downloads

Contains my minecraft mods folder, my social media icon, and my minecraft saves folder.

### Games

Contains a few games made by me and eventually a bunch of other people. Credit to [Scratch](https://scratch.mit.edu) for the game creation tool.

### Drawings

The newest section! I put random drawings in here along with their title. Most will be clickable, going to their facebook link.

## Credits

### [W3Schools](https://w3schools.com)

Thanks to W3Schools for their HTML and CSS tutorials, as well as their [Try it Editor](https://www.w3schools.com/code/tryit.asp?filename=FU8LPIM0L0QM).
### [Scratch](https://www.scratch.mit.edu)

Thanks go to Scratch for the game creation tool!
